"use strict";

// ## Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

// Екранування за допомогою зворотньго слеша додає спецсимволи у рядки. Ці спецсимволи дають можливість переносити рядки в нові строки \n,
// додавати лапку \" \', додати сам зворотній слеш \\, додати юнікодні коди та інше.

// 2. Які засоби оголошення функцій ви знаєте?

// Function declaration. Функція оголошується за допомогою ключового слова function fn (){}.
// Function expression. Функція також оголошується за допомогою ключового слова function і записується в змінну const fn = function (){}.
// Стрілкова функція. Записується в змінну const fn = (a,b) => a+b.

// 3. Що таке hoisting, як він працює для змінних та функцій?

// Js пробігається по коду і запамятовує які є функціі та оголошенні змінні. Функція, якщо вона не записанна в змінну, 
// може виконуватись в будь-якій частині коду там де вона викликана, навіть до запису самої функціі. Змінним може присвоюватись значення
// та використовуватись до їх обяви, але вони не можуть використовуватись без присвоєння значення. Якщо змінній присвоюється значення після 
// використання, то при використанні буде undefined.

// ## Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.

function createNewUser() {
  let userFirstName = prompt("Enter your name");
  let userLastName = prompt("Enter your lastname");
  let userBirthday = prompt("Enter your birthday", ["dd.mm.yyyy"]);

  const newUser = {};
 
  newUser["firstName"] = userFirstName;
  newUser["lastName"] = userLastName;
  newUser["birthday"] = userBirthday;

  newUser["getAge"] = function () {
    let birthdayDate = new Date(userBirthday.split(".").reverse().join("-"));
    return Math.trunc((Date.now() - +birthdayDate) / 31536e6);
  };

  newUser["getLogin"] = function () {
    return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase();
  };

  newUser["getPassword"] = function () {
    let birthdayDate = new Date(userBirthday.split(".").reverse().join("-"));
    return (
      newUser.firstName[0].toUpperCase() +
      newUser.lastName.toLowerCase() +
      birthdayDate.getFullYear()
    );
  };

  return newUser;
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

